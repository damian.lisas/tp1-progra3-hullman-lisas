package exception;

public class JuegoTerminadoException extends Exception {

	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = 8611449639823226336L;
	
	public JuegoTerminadoException() {
		super();
	}
	
	public JuegoTerminadoException(Object mensaje) {
		super(mensaje.toString());
	}

}
