package exception;

public class NivelDeDificultadInvalidoException extends Exception {

	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = -6603603277723006309L;
	
	public NivelDeDificultadInvalidoException() {
		super();
	}
	
	public NivelDeDificultadInvalidoException(Object mensaje) {
		super(mensaje.toString());
	}
	
}
