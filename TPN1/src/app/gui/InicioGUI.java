package app.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import app.gui.frame.Pantalla;
import app.gui.panel.Panel;
import app.gui.panel.PanelJuego;
import app.gui.panel.PanelMenu;

/**
 * Pantalla principal de la aplicacion
 * 
 * @author Damian
 */
public class InicioGUI {

	private Pantalla frame;
	private PanelMenu panelMenu; 
	private PanelJuego panelJuego;

	/**
	 * Launch the application.
	 */
	public static void correr() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InicioGUI window = new InicioGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InicioGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		panelMenu = new PanelMenu();
		panelJuego = new PanelJuego();
		
		frame = new Pantalla(panelMenu);
		frame.setBounds(100, 100, 605, 810);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panelMenu.getBtnJugar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.mostrar(panelJuego);
			}
		});
		
		panelMenu.getBtnSalir().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});
		
		panelJuego.getBtnAbandonar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.mostrar(panelMenu);
			}
		});
	}
}
