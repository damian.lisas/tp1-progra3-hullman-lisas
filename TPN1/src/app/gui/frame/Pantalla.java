package app.gui.frame;

import javax.swing.JFrame;

import app.gui.panel.Panel;

/**
 * Pantalla de la aplicacion<br>
 * La pantalla debe contener 1 solo panel a la vez para poder realizar la navegacion entre estos de manera correcta
 * 
 * @author dergo
 */
public class Pantalla extends JFrame {

	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = 885806655916503097L;
	
	/**
	 * Crea una pantalla y le a�ade el panel
	 * 
	 * @param panel Panel con el que se va a crear la pantalla
	 */
	public Pantalla(Panel panel) {
		super();
		
		this.getContentPane().add(panel);
	}
	
	/**
	 * Reemplaza el panel actual por el panel que se le pasa por parametro
	 * 
	 * @param panel Panel que se quiere mostrar
	 */
	public void mostrar(Panel panel) {
		Panel panelActual = (Panel) this.getContentPane().getComponent(0);
		
		panelActual.cerrar();
		this.getContentPane().remove(panelActual);
		
		panel.abrir();
		this.getContentPane().add(panel);
	}
}
