package app.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import app.controlador.ControladorJuego;
import util.Tupla;

public class InitGUI {
	
	private JFrame frame;
	private JPanel contenedor;
	private JPanel panelJuego;
	
	private JButton btnAbandonar;
	private JButton btnJugar;
	private JButton btnSalir;
	
	private JTextField inputNombreJugador;
	
	public void correr() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InitGUI window = new InitGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
	public InitGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		
		frame.setLayout(null);
		frame.setBounds(100, 100, 605, 800);
		frame.setResizable(false);
		frame.setTitle("Lights Out! | TP N� 1 Programaci�n III");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		generarMenuPrincipal();
	
	}
	
	public void generarMenuPrincipal() {
		if(contenedor != null) {
			frame.getContentPane().remove(contenedor);
		}
		contenedor = new JPanel();
		contenedor.setBounds(0, 0, 600, 800);
		contenedor.setBackground(Color.WHITE);
		contenedor.setLayout(new BorderLayout());
		
		/* PANEL NORTE */
		JPanel panelNorte = new JPanel();
		
		JLabel titulo = new JLabel("Lights Out!\n GAME");
		titulo.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
		titulo.setHorizontalAlignment(JLabel.CENTER);
		titulo.setFont(new Font("Heattenschweiler", Font.PLAIN, 28));
		titulo.setPreferredSize(new Dimension(300, 250));
		panelNorte.add(titulo);
		
		contenedor.add(panelNorte, BorderLayout.NORTH);
		
		/* PANEL CENTRAL */
		JPanel panelCentral = new JPanel();
		
		JLabel nombreDeJugador = new JLabel("Nombre del Jugador:");
		panelCentral.add(nombreDeJugador);
		
		inputNombreJugador = new JTextField();
		inputNombreJugador.setPreferredSize(new Dimension(500, 25));
		inputNombreJugador.setMaximumSize(new Dimension(500, 25));
		panelCentral.add(inputNombreJugador);
		
		
		btnJugar = new JButton("Jugar");
		btnJugar.setPreferredSize(new Dimension(500, 25));
		btnJugar.setMaximumSize(new Dimension(500, 25));
		btnJugar.setBackground(Color.LIGHT_GRAY);
		btnJugar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(inputNombreJugador.getText().isEmpty()) {
					JOptionPane.showMessageDialog(contenedor, "Ten�s que ingresar tu nombre!");
					return;
				}
				ControladorJuego.setNombreJugador(inputNombreJugador.getText());
				generarPanelDeJuego();
			}
		});
		panelCentral.add(btnJugar);
		
		btnSalir = new JButton("Salir");
		btnSalir.setPreferredSize(new Dimension(500, 25));
		btnSalir.setMaximumSize(new Dimension(500, 25));
		btnSalir.setBackground(Color.LIGHT_GRAY);
		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panelCentral.add(btnSalir);
		
		JLabel textoPiePagina = new JLabel("Damian Lisas | Nicolas Rodriguez");
		textoPiePagina.setHorizontalAlignment(JLabel.CENTER);
		textoPiePagina.setBorder(BorderFactory.createEmptyBorder(300, 30, 30, 30));
		panelCentral.add(textoPiePagina);
		
		contenedor.add(panelCentral, BorderLayout.CENTER);
		
		frame.getContentPane().add(contenedor);
		frame.revalidate();
		frame.repaint();
	}
	
	public void generarPanelDeJuego() {
		if(contenedor != null) {
			frame.getContentPane().remove(contenedor);
		}
		contenedor = new JPanel();
		
		btnAbandonar = new JButton("Abandonar");
		
		contenedor.setBounds(0, 0, 600, 800);
		contenedor.setBackground(Color.WHITE);
		contenedor.setLayout(new BoxLayout(contenedor, BoxLayout.Y_AXIS));
		
		/* PANEL NORTE */
		JPanel header = new JPanel();
		header.setPreferredSize(new Dimension(600, 50));
		header.setMaximumSize(new Dimension(600, 50));
		
		JLabel nombreJugador = new JLabel(ControladorJuego.getNombreJugador());
		nombreJugador.setHorizontalAlignment(JLabel.LEFT);
		nombreJugador.setFont(new Font("Calibri", Font.PLAIN, 25));
		nombreJugador.setPreferredSize(new Dimension(520, 25));
		nombreJugador.setMaximumSize(new Dimension(520, 25));
		header.add(nombreJugador);
		
		JLabel nivel = new JLabel(ControladorJuego.getNombreNivelActual());
		nivel.setHorizontalAlignment(JLabel.LEFT);
		nivel.setFont(new Font("Calibri", Font.PLAIN, 12));
		nivel.setPreferredSize(new Dimension(500, 15));
		nivel.setMaximumSize(new Dimension(500, 15));
		header.add(nivel);
		
		contenedor.add(header);
		
		/* PANEL CENTRAL */
		panelJuego = new JPanel();
		panelJuego.setPreferredSize(new Dimension(600, 600));
		panelJuego.setMaximumSize(new Dimension(600, 600));
		panelJuego.setBackground(Color.LIGHT_GRAY);
		panelJuego.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelJuego.setLayout(new GridLayout(0, ControladorJuego.getTamanioTablero()));
		
		for(int i = 0 ; i < ControladorJuego.getTablero().length ; i++) {
			for(int j = 0 ; j < ControladorJuego.getTablero()[i].length ; j++) {
				LuzGUI luz = new LuzGUI();
				if(ControladorJuego.getTablero()[i][j]) {
					luz.setBackground(Color.YELLOW);
				}else {
					luz.setBackground(Color.GRAY);
				}
				luz.setPosicion(new Tupla<Integer, Integer>(i, j));
				final int y = i;
				final int x = j;
				luz.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ControladorJuego.cambiarEstadoLuzYVicinas(x, y);
						ControladorJuego.actualizarVista();
						if(ControladorJuego.ganoElNivel()) {
							JOptionPane.showMessageDialog(contenedor, "Felicitaciones! Pasaste al siguiente Nivel");
							ControladorJuego.siguienteNivel();
						}
					}
				});
				
				panelJuego.add(luz);
			}
		}
		
		contenedor.add(panelJuego);
		
		/* PANEL SUR */
		JPanel footer = new JPanel();
		btnAbandonar = new JButton("Abandonar");
		btnAbandonar.setBackground(Color.WHITE);
		btnAbandonar.setBorder(BorderFactory.createEmptyBorder(25, 25, 25, 25));
		btnAbandonar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer opcionSeleccionada = JOptionPane.showConfirmDialog(contenedor, "Te das por vencido?");
				if(opcionSeleccionada.equals(0)) {
					ControladorJuego.abandonoElJuego();
				}
			}
		});
		footer.add(btnAbandonar);
		contenedor.add(footer);
		
		frame.getContentPane().add(contenedor);
		frame.revalidate();
		frame.repaint();
	}
	
	public void getMensajeAlGanador() {
		JOptionPane.showMessageDialog(contenedor, "FELICITACIONES! GANASTE LIGHTSOUT!");
	}

}
