package app.gui.panel;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 * El panel es una vista de la aplicacion. Una aplicacion puede tener diferentes paneles e ir alternando entre ellos para mostrar diferentes funcionalidades<br>
 * Se asume que el panel va a utilizar SpringLayout<br>
 * 
 * @author Damian
 */
public abstract class Panel extends JPanel{
	
	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = 4791844770817333615L;
	
	protected SpringLayout layout;
	
	public Panel() {
		this.inicializar();
	}
	
	public void actualizarPanel() {
		this.inicializar();
	}

	protected void inicializar() {
		this.abrir();
		
		this.layout = new SpringLayout();
		this.setLayout(layout);
		
		this.inticializarComponentes();
		this.setConstraints();
	}
	
	public void abrir() {
		this.setVisible(true);
	}
	
	public void cerrar() {
		this.setVisible(false);
	}
	
	
	protected abstract void setConstraints();
	
	protected abstract void inticializarComponentes();
}
