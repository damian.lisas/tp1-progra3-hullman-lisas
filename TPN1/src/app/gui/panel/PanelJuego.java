package app.gui.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.controlador.ControladorJuego;
import app.gui.InicioGUI;
import app.gui.LuzGUI;
import util.Tupla;

/**
 * Panel principal del juego
 * 
 * @author Nicolas
 */
public class PanelJuego extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4104977976410328747L;
	
	private JButton btnAbandonar;
	private JPanel contenedor;
	
	public JButton getBtnAbandonar() {
		return this.btnAbandonar;
	}

	public void setBtnAbandonar(JButton boton) {
		this.btnAbandonar = boton;
	}

	@Override
	protected void inticializarComponentes() {
		btnAbandonar = new JButton("Abandonar");
		
		contenedor = new JPanel();
		contenedor.setBounds(0, 0, 600, 800);
		contenedor.setBackground(Color.WHITE);
		contenedor.setLayout(new BoxLayout(contenedor, BoxLayout.Y_AXIS));
		
		/* PANEL NORTE */
		JPanel header = new JPanel();
		header.setPreferredSize(new Dimension(600, 50));
		header.setMaximumSize(new Dimension(600, 50));
		
		JLabel nombreJugador = new JLabel("Nicolas");
		nombreJugador.setHorizontalAlignment(JLabel.LEFT);
		nombreJugador.setFont(new Font("Calibri", Font.PLAIN, 25));
		nombreJugador.setPreferredSize(new Dimension(500, 25));
		nombreJugador.setMaximumSize(new Dimension(500, 25));
		header.add(nombreJugador);
		
		JLabel nivel = new JLabel("Nivel: 5");
		nivel.setHorizontalAlignment(JLabel.LEFT);
		nivel.setFont(new Font("Calibri", Font.PLAIN, 12));
		nivel.setPreferredSize(new Dimension(500, 15));
		nivel.setMaximumSize(new Dimension(500, 15));
		header.add(nivel);
		
		contenedor.add(header);
		
		/* PANEL CENTRAL */
		JPanel body = new JPanel();
		body.setPreferredSize(new Dimension(600, 600));
		body.setMaximumSize(new Dimension(600, 600));
		body.setBackground(Color.LIGHT_GRAY);
		body.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		body.setLayout(new GridLayout(0, ControladorJuego.getTamanioTablero()));
		contenedor.add(body);
		
		for(int i = 0 ; i < ControladorJuego.getTablero().length ; i++) {
			for(int j = 0 ; j < ControladorJuego.getTablero()[i].length ; j++) {
				LuzGUI luz = new LuzGUI();
				if(ControladorJuego.getTablero()[i][j]) {
					luz.setBackground(Color.YELLOW);
				}else {
					luz.setBackground(Color.GRAY);
				}
				luz.setPosicion(new Tupla<Integer, Integer>(i, j));
				final int y = i;
				final int x = j;
				luz.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ControladorJuego.cambiarEstadoLuzYVicinas(x, y);
					}
				});
				
				body.add(luz);
			}
		}
		
		/* PANEL SUR */
		JPanel footer = new JPanel();
		btnAbandonar = new JButton("Abandonar");
		btnAbandonar.setBackground(Color.WHITE);
		footer.add(btnAbandonar);
		contenedor.add(footer);
		
		this.add(contenedor);
	}

	@Override
	protected void setConstraints() {
		
	}
}
