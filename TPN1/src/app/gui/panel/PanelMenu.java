package app.gui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import app.controlador.ControladorJuego;

/**
 * Panel que contiene las diferentes funciones que puede realizar el usuario al ingresar a la aplicacion
 * 
 * @author Nicolas
 */
public class PanelMenu extends Panel{

	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel contenedor;
	private JTextField inputNombreJugador;
	private JButton btnJugar, btnSalir;

	public JButton getBtnJugar() {
		return this.btnJugar;
	}

	public void setBtnJugar(JButton btnJugar) {
		this.btnJugar = btnJugar;
	}

	public JButton getBtnSalir() {
		return this.btnSalir;
	}

	public void setBtnSalir(JButton btnSalir) {
		this.btnSalir = btnSalir;
	}
	
	@Override
	protected void inticializarComponentes() {
		contenedor = new JPanel();
		this.setLayout(null);
		contenedor.setBounds(0, 0, 600, 800);
		contenedor.setBackground(Color.WHITE);
		contenedor.setLayout(new BorderLayout());
		
		/* PANEL NORTE */
		JPanel panelNorte = new JPanel();
		
		JLabel titulo = new JLabel("Lights Out!\n GAME");
		titulo.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
		titulo.setHorizontalAlignment(JLabel.CENTER);
		titulo.setFont(new Font("Heattenschweiler", Font.PLAIN, 28));
		titulo.setPreferredSize(new Dimension(300, 250));
		panelNorte.add(titulo);
		
		contenedor.add(panelNorte, BorderLayout.NORTH);
		
		/* PANEL CENTRAL */
		JPanel panelCentral = new JPanel();
		
		JLabel nombreDeJugador = new JLabel("Nombre del Jugador:");
		panelCentral.add(nombreDeJugador);
		
		inputNombreJugador = new JTextField();
		inputNombreJugador.setPreferredSize(new Dimension(500, 25));
		inputNombreJugador.setMaximumSize(new Dimension(500, 25));
		panelCentral.add(inputNombreJugador);
		
		
		btnJugar = new JButton("Jugar");
		btnJugar.setPreferredSize(new Dimension(500, 25));
		btnJugar.setMaximumSize(new Dimension(500, 25));
		btnJugar.setBackground(Color.LIGHT_GRAY);
		btnJugar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ControladorJuego.setNombreJugador(inputNombreJugador.getText());
			}
		});
		panelCentral.add(btnJugar);
		
		btnSalir = new JButton("Salir");
		btnSalir.setPreferredSize(new Dimension(500, 25));
		btnSalir.setMaximumSize(new Dimension(500, 25));
		btnSalir.setBackground(Color.LIGHT_GRAY);
		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panelCentral.add(btnSalir);
		
		contenedor.add(panelCentral, BorderLayout.CENTER);
		
		/* PANEL SUR */
		JPanel panelSur = new JPanel();
		
		JLabel textoPiePagina = new JLabel("Trabajo Práctico N° 1 | Programación III | 2019");
		textoPiePagina.setHorizontalAlignment(JLabel.CENTER);
		panelSur.add(textoPiePagina);
		
		contenedor.add(panelSur, BorderLayout.SOUTH);
		
		this.add(contenedor);
	}

	@Override
	protected void setConstraints() {
	}
}
