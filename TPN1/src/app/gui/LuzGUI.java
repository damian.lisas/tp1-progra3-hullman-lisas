package app.gui;

import javax.swing.JButton;

import util.Tupla;

public class LuzGUI extends JButton {
	/**
	 * Auto generado
	 */
	private static final long serialVersionUID = -2863514038940212686L;
	
	private Tupla<Integer, Integer> coordenadas;
	
	public LuzGUI() {
		super();
		this.setPosicion(new Tupla<Integer, Integer>(0, 0));
	}
	
	public LuzGUI(Integer x, Integer y, String nombre) {
		super(nombre);
		this.setPosicion(new Tupla<Integer, Integer>(x, y));
	}
	
	public LuzGUI(Tupla<Integer, Integer> posicion) {
		super();
		this.setPosicion(posicion);
	}

	public Tupla<Integer, Integer> getPosicion() {
		return coordenadas;
	}

	public void setPosicion(Tupla<Integer, Integer> posicion) {
		this.coordenadas = posicion;
	}
}
