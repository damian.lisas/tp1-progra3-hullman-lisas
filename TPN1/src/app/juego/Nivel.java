package app.juego;

import java.util.HashMap;

import com.sun.javafx.collections.MappingChange.Map;

public enum Nivel {
	
	UNO ("Nivel 1", 3),
	DOS ("Nivel 2", 4), 
	TRES ("Nivel 3", 5),
	CUATRO ("Nivel 4", 6),
	CINCO ("Nivel 5", 7),
	SEIS ("Nivel 6", 8),
	SIETE ("Nivel 7", 9),
	OCHO ("Nivel 8", 10),
	NUEVE ("Nivel 9", 11),
	DIEZ ("Nivel 10", 12),
	GANADOR ("", 13);
	
	private String nombreNivel;
	private int tamanioTablero;
	
	private Nivel(String nombreNivel, int tamanioTablero) {
		this.nombreNivel = nombreNivel;
		this.tamanioTablero = tamanioTablero;
	}

	public String getNombreNivel() {
		return nombreNivel;
	}

	public void setNombreNivel(String nombreNivel) {
		this.nombreNivel = nombreNivel;
	}

	public int getTamanioTablero() {
		return tamanioTablero;
	}

	public void setTamanioTablero(int tamanioTablero) {
		this.tamanioTablero = tamanioTablero;
	}
	
	public Nivel getSiguiente(Nivel nivel) {
		int nuevoTamanio = nivel.getTamanioTablero() - 1;
		nivel.setNombreNivel("Nivel " + nuevoTamanio);
		nivel.setTamanioTablero(nivel.getTamanioTablero() + 1);
		return nivel;
	}

}
