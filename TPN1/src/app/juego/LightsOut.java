package app.juego;

import java.util.List;

import app.juego.Dificultad.NIVEL;
import app.logic.tablero.Tablero;
import exception.JuegoTerminadoException;
import exception.NivelDeDificultadInvalidoException;
import util.Tupla;

public class LightsOut {
	
	private final int CANTIDAD_LUCES_ENCENDIDAS_PARA_GANAR = 0;
	
	private final boolean ENCENDIDA = true;
	
	private final int TAMANO_POR_DEFECTO = 3;
	
	private final NIVEL DIFICULTAD_POR_DEFECTO = Dificultad.NIVEL.FACIL;

	private Tablero tablero;
	private Puntaje puntaje;
	private Dificultad dificultad;
	private int lucesEncendidas;
	private int tamano;
	private boolean hayGanador;
	private String nombreJugador;
	private Nivel nivel;
	
	/**
	 * Crea un juego con un talblero de 3x3
	 */
	public LightsOut() {
		this.crearJuego(TAMANO_POR_DEFECTO, DIFICULTAD_POR_DEFECTO);
	}
	
	public LightsOut(int tamano) {
		this.crearJuego(tamano, DIFICULTAD_POR_DEFECTO);
	}
	
	public LightsOut(int tamano, NIVEL dificultad) {
		this.crearJuego(tamano, dificultad);
	}
	
	public LightsOut(Nivel nivel) {
		this.nivel = nivel;
		this.crearJuego(nivel);
	}
	
	public LightsOut(Tablero tablero) {
		this.dificultad = new Dificultad(DIFICULTAD_POR_DEFECTO);
		this.puntaje = new Puntaje();
		this.tamano = tablero.tamano();
		this.tablero = tablero;
		this.lucesEncendidas = tablero.contarPosicionesEnTrue();
		this.hayGanador = false;
	}
	
	/**
	 * Apaga la luz indicada en la fila-columna y luego establece si hubo un ganador despues de este turno
	 * 
	 * @param fila Fila donde se encuentra la luz
	 * @param columna Columna donde se encuentra la luz
	 * @throws JuegoTerminadoException si se intenta realizar una jugada cuando el juego ya finalizo
	 */
	public void jugar(int fila, int columna) throws JuegoTerminadoException {
		
		if(this.hayGanador == true) {
			throw new JuegoTerminadoException("No se puede realizar una jugada en un juego finalizado");
		}
		
		this.tablero.alternarConVecinos(fila, columna);
		this.setearNuevoPuntaje();
		this.chequearVicotria(fila, columna);
	}
	
	/**
	 * Devuelve si el juego tuvo un ganador
	 * @return
	 */
	public boolean hayGanador() {
		return this.hayGanador;
	}
	
	/**
	 * Devuelve la cantidad de luces encendidas en el tablero
	 * @return int cantidad de luces encendidas en el tablero
	 */
	public int getCantidadLucesEncendidas() {
		return this.lucesEncendidas;
	}
	
	public int getPuntaje() {
		return this.puntaje.getCantidad();
	}

	public void setPuntaje(int puntaje) {
		this.puntaje.setCantidad(puntaje);
	}
	
	public int getDificultad() {
		return this.dificultad.getDificultad();
	}
	
	public void setDificultad(NIVEL dificultad) {
		this.dificultad.setDificultad(dificultad);
		this.puntaje.setVariacion(puntaje.getVariacionBase() * this.getDificultad());
	}
	
	private void setearNuevoPuntaje() {
		this.setPuntaje(calcularNuevoPuntaje());;
	}
	
	private int calcularNuevoPuntaje() {
		int variacion = (this.puntaje.getVariacion() * (this.tablero.tamano() + this.getCantidadLucesEncendidas())) * this.getDificultad();
		int nuevoPuntaje = this.puntaje.getCantidad() - variacion;
		
		return nuevoPuntaje;
	}
	
	/**
	 * Crea un juego con un talbero de tama�o personalizado<br>
	 * Se asegura que el tablero que se genera tenga al menos 1 luz encendida para que se pueda jugar
	 * 
	 * @param tamano El tama�o del tablero de juego
	 * @throws NivelDeDificultadInvalidoException 
	 */
	private void crearJuego(int tamano, NIVEL dificultad) {
		this.puntaje = new Puntaje();
		this.dificultad = new Dificultad(dificultad);
		this.tamano = tamano;
		this.lucesEncendidas = 0;
		
		while(this.lucesEncendidas == CANTIDAD_LUCES_ENCENDIDAS_PARA_GANAR) {
			this.tablero = new Tablero(this.tamano, Tablero.GENERACION.ALEATORIA);
			this.lucesEncendidas = tablero.contarPosicionesEnTrue();
		}

		this.hayGanador = false;
	}
	
	private void crearJuego(Nivel nivel) {
		this.tablero = new Tablero(nivel.getTamanioTablero(), Tablero.GENERACION.ALEATORIA);
		
		while(verificarGanador()) {
			crearJuego(nivel);
		}
	}
	
	public boolean verificarGanador() {
		boolean ganador = false;
		int contador = 0;
		for(int i = 0 ; i < this.tablero.getTablero().length ; i++) {
			for(int j = 0 ; j < this.tablero.getTablero()[i].length ; j++) {
				if(this.tablero.getTablero()[i][j]) {
					contador++;
				}
			}
		}
		
		if(contador == this.tablero.getTamano() * this.tablero.getTamano()) {
			ganador = true;
		}
		
		return ganador;
	}
	
	/**
	 * Establece si hubo un ganador luego de la jugada anterior revisando el estado de los vecinos adyacentes
	 * 
	 * @param fila Fila en la que se encuentra la luz
	 * @param columna Columna en la que se encuentra la luz
	 */
	private void chequearVicotria(int fila, int columna) {
		List<Tupla<Integer, Integer>> vecinos = tablero.getVecinos(fila, columna);
		
		//chequea el estado de los vecinos
		for(Tupla<Integer, Integer> vecino : vecinos) {
			if(tablero.getContenido(vecino.x, vecino.y) == ENCENDIDA) {//Si esta encendida implica que en el turno anterior estaba apagada
				this.lucesEncendidas++;
			} else {
				this.lucesEncendidas--;
			}
		}
		//chequea el estado de la luz que se alterno
		if(tablero.getContenido(fila, columna) == ENCENDIDA) {
			this.lucesEncendidas++;
		} else {
			this.lucesEncendidas--;
		}
		
		if(this.lucesEncendidas == CANTIDAD_LUCES_ENCENDIDAS_PARA_GANAR) {
			this.hayGanador = true;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{\n");
		builder.append("	Hay ganador = "+this.hayGanador()+"\n");
		builder.append("	Cantidad luces encendidas = "+this.lucesEncendidas+"\n");
		builder.append("	Cantidad luces encendidas para ganar = "+this.CANTIDAD_LUCES_ENCENDIDAS_PARA_GANAR+"\n");
		builder.append("	Tablero:\n");
		builder.append(this.tablero.toString());
		builder.append("}\n");
		
		return builder.toString();
	}

	public String getNombreJugador() {
		return nombreJugador;
	}

	public void setNombreJugador(String nombreJugador) {
		StringBuilder posibleResultado = new StringBuilder();
		if(nombreJugador.length() > 15) {
			for(int i = 0 ; i < 15 ; i++) {
				posibleResultado.append(nombreJugador.charAt(i));
			}
			
			this.nombreJugador = posibleResultado.toString();
		}
		else {
			this.nombreJugador = nombreJugador;
		}
	}

	public Tablero getTablero() {
		return tablero;
	}

	public void setTablero(Tablero tablero) {
		this.tablero = tablero;
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}
	
}
