package app.juego;

public class Dificultad {
	
	public enum NIVEL {FACIL, MEDIO, DIFICIL, ABURRIDO};
	
	private int dificultad;
	
	public Dificultad(NIVEL dificultad) {
		this.dificultad = this.valueOf(dificultad);
	}

	public int getDificultad() {
		return this.dificultad;
	}
	
	public void setDificultad(NIVEL dificultad) {
		this.dificultad = valueOf(dificultad);
	}
	
	private int valueOf(NIVEL dificultad) {
		switch(dificultad) {
		case FACIL:
			return 1;
		case MEDIO:
			return 2;
		case DIFICIL:
			return 4;
		case ABURRIDO:
			return 25;
		default:
			return 1;
		}
	}
}
