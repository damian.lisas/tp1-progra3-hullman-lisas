package app.juego;

public class Puntaje {
	
	private final int CANTIDAD_POR_DEFECTO = 10000;
	private final int VARIACION_POR_DEFECTO = 20;
	
	private int cantidad;
	
	private int variacion;
	
	public Puntaje() {
		this.cantidad = this.CANTIDAD_POR_DEFECTO;
		this.variacion = this.VARIACION_POR_DEFECTO;
	}
	
	public Puntaje(int cantidadInicial, int variacion) {
		this.cantidad = cantidadInicial;
		this.variacion = variacion;
	}
	
	public int getCantidad() {
		return this.cantidad;
	}
	
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public int getVariacion() {
		return this.variacion;
	}

	public int getVariacionBase() {
		return this.VARIACION_POR_DEFECTO;
	}

	public void setVariacion(int variacion) {
		this.variacion = variacion;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("{\n");
		builder.append("	cantidad = "+this.getCantidad()+"\n");
		builder.append("	variacion = "+this.getVariacion()+"\n");
		builder.append("{\n");
		
		return builder.toString();
	}
}
