package app.controlador;

import app.gui.InitGUI;
import app.juego.LightsOut;
import app.juego.Nivel;

public class ControladorJuego {
	
	private static LightsOut juego;
	private static InitGUI GUI;
	
	public static void main(String[] args) {
		juego = new LightsOut(Nivel.UNO);
		GUI = new InitGUI();
	}

	public static LightsOut getJuego() {
		return juego;
	}

	public static void setJuego(LightsOut juego) {
		ControladorJuego.juego = juego;
	}

	public static int getTamanioTablero() {
		return juego.getTablero().getTamano();
	}
	
	public static void setNombreJugador(String nombreJugador) {
		juego.setNombreJugador(nombreJugador);
	}
	
	public static String getNombreJugador() {
		return juego.getNombreJugador();
	}
	
	public static boolean[][] getTablero(){
		return juego.getTablero().getTablero();
	}
	
	public static void cambiarEstadoLuzYVicinas(int x, int y) {
		juego.getTablero().alternar(x, y);
		juego.getTablero().alternarVecinos(x, y);
	}
	
	public static void actualizarVista() {
		GUI.generarPanelDeJuego();
	}
	
	public static boolean ganoElNivel() {
		return juego.verificarGanador();
	}
	
	public static void siguienteNivel() {
		String nombreDelJugador = juego.getNombreJugador();
		juego = new LightsOut(juego.getNivel().getSiguiente(juego.getNivel()));
		juego.setNombreJugador(nombreDelJugador);
		if(juego.getNivel().equals(Nivel.GANADOR)) {
			GUI.getMensajeAlGanador();
			ControladorJuego.ganoElJuego();
		}
		GUI.generarPanelDeJuego();
	}
	
	public static String getNombreNivelActual() {
		return juego.getNivel().getNombreNivel();
	}
	
	public static void abandonoElJuego() {
		juego = new LightsOut(Nivel.UNO);
		GUI.generarMenuPrincipal();
	}
	
	public static void ganoElJuego() {
		System.exit(0);
	}

}
