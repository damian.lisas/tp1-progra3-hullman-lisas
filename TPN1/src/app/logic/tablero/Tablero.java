package app.logic.tablero;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import util.Tupla;

public class Tablero implements Comparable<Tablero>{

	/**
	 * Representacion del tablero
	 */
	private boolean[][] tablero;

	/**
	 * Largo y ancho del tablero. EL tablero tiene el mismo largo que ancho
	 */
	private int tamano;
	
	public static enum GENERACION {ALEATORIA, ENCENDIDO, ALTERNADO};

	/**
	 * Crea un tablero de tama�o x tama�o con todos sus elementos en false
	 * 
	 * @param tamano Cantidad de posiciones que va a tener, tanto de largo como de
	 *               ancho
	 */
	public Tablero(int tamano) {
		this.tablero = new boolean[tamano][tamano];
		this.tamano = tamano;
	}
	
	public Tablero(int tamano, GENERACION tipoDeGeneracion) {
		this.tamano = tamano;
		
		switch (tipoDeGeneracion) {
		case ALEATORIA:
			this.tablero = generarMatrizAleatoria(tamano);
			break;
		case ENCENDIDO:
			this.tablero = generarMatrizEncendida(tamano);
			break;
		case ALTERNADO:
			this.tablero = generarMatrizAlternada(tamano);
			break;
		default:
			this.tablero = new boolean[tamano][tamano];
			break;
		}
		
	}

	/**
	 * Devuelve el contenido de la posicion fila-columna
	 * 
	 * @param fila    Fila del tablero
	 * @param columna Columna del tablero
	 * @return El contenido de la posicion indicada
	 */
	public boolean getContenido(int fila, int columna) {
		if(!this.existe(fila, columna)) {
			throw new IndexOutOfBoundsException("No se puede obtener el contenido de un elemento que no esta en el tablero. Fila = " + fila + ", Columna = " + columna);
		}
		
		return this.tablero[fila][columna];
	}

	/**
	 * Alterna el contenido de la posicion fila-columna
	 * 
	 * @param fila    Fila del tablero
	 * @param columna Columna del tablero
	 */
	public void alternar(int columna, int fila) {
		if(!this.existe(fila, columna)) {
			throw new IndexOutOfBoundsException("No se puede alternar un elemento que no esta en el tablero. Fila = " + fila + ", Columna = " + columna);
		}
		
		this.tablero[fila][columna] = !getContenido(fila, columna);
	}
	
	/**
	 * Alterna el elemento seleccionado y sus vecinos
	 * 
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemnto
	 */
	public void alternarConVecinos(int fila, int columna) {
		this.alternar(fila, columna);
		this.alternarVecinos(fila, columna);
	}
	
	/**
	 * Alterna los vecinos de un elemento
	 * 
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemento
	 */
	public void alternarVecinos(int fila, int columna) {
		List<Tupla<Integer, Integer>> vecinos = getVecinos(fila, columna);

		for(Tupla<Integer, Integer> tupla : vecinos) {
			this.alternar(tupla.x, tupla.y);
		}
	}

	/**
	 * Devuelve los vecinos de un elemento en el tablero. La fila y la columna
	 * defienen el elemento del que se quieren buscar los vecinos
	 * 
	 * @param fila    Fila en el tablero
	 * @param columna Columna en el tablero
	 * @return Map con los vecinos del elemento
	 */
	public List<Tupla<Integer, Integer>> getVecinos(int fila, int columna) {
		//             X
		// Vecinos = X O X
		//             X
		List<Tupla<Integer, Integer>> vecinos = new ArrayList<Tupla<Integer, Integer>>();
		
		
		// Agrego los vecinos
		vecinos = agregarVecinoSuperior(vecinos, fila, columna);
		vecinos = agregarVecinoDerecho(vecinos, fila, columna);
		vecinos = agregarVecinoIzquierdo(vecinos, fila, columna);
		vecinos = agregarVecinoInferior(vecinos, fila, columna);

		return vecinos;
	}
	
	/**
	 * Devuelve el tama�o del tablero
	 * 
	 * @return El tama�o del tablero
	 */
	public int tamano() {
		return this.tamano;
	}
	
	/**
	 * Devuelve True si el elemento puede ser encontrado dentro del tablero
	 * 
	 * @param fila Fila en la que se debe encontrar el elemento
	 * @param columna Columna en la que se debe encontrar el elemento
	 * @return True si el elemento puede ser encontrado dentro del tablero
	 * @return False si el elemento no puede ser encontrado dentro del tablero
	 */
	public boolean existe(int fila, int columna) {
		if(fila < 0 || fila >= this.tamano() || columna < 0 || columna >= this.tamano()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Devuelve la cantidad de elementos del tablero que son true
	 * 
	 * @return int cantidad de elementos en true
	 */
	public int contarPosicionesEnTrue() {
		int cantidadTrue = 0;
		
		for(int i = 0; i < this.tamano(); i++) {
			for(int k = 0; k < this.tamano(); k++) {
				if(this.getContenido(i, k) == true) {
					cantidadTrue++;
				}
			}
		}
		
		return cantidadTrue;
	}

	/**
	 * Agrega el vecino superior del elemento definido por la fila y la columna en el Map de vecinos si existe
	 * 
	 * @param vecinos Map de vecinos
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemento
	 * @return Map de vecinos con el vecino superior si existe
	 */
	private List<Tupla<Integer, Integer>> agregarVecinoSuperior(List<Tupla<Integer, Integer>> vecinos, int fila, int columna) {
		int filaVecino = fila - 1;
		int columnaVecino = columna;
		
		// Si el vecino no existe devuleve el map actual de vecinos
		if(!this.existe(filaVecino, columnaVecino)) {
			return vecinos;
		}
		
		// Agrego el vecino superior
		vecinos.add(new Tupla<Integer, Integer>(filaVecino, columnaVecino));
		
		return vecinos;
	}
	
	/**
	 * Agrega el vecino derecho del elemento definido por la fila y la columna en el Map de vecinos si existe
	 * 
	 * @param vecinos Map de vecinos
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemento
	 * @return Map de vecinos con el vecino derecho si existe
	 */
	private List<Tupla<Integer, Integer>> agregarVecinoDerecho(List<Tupla<Integer, Integer>> vecinos, int fila, int columna) {
		int filaVecino = fila;
		int columnaVecino = columna + 1;
		
		// Si el vecino no existe devuleve el map actual de vecinos
		if(!this.existe(filaVecino, columnaVecino)) {
			return vecinos;
		}
		
		// Agrego el vecino derecho
		vecinos.add(new Tupla<Integer, Integer>(filaVecino, columnaVecino));
		
		return vecinos;
	}
	
	/**
	 * Agrega el vecino izquierdo del elemento definido por la fila y la columna en el Map de vecinos si existe
	 * 
	 * @param vecinos Map de vecinos
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemento
	 * @return Map de vecinos con el vecino izquierdo si existe
	 */
	private List<Tupla<Integer, Integer>> agregarVecinoIzquierdo(List<Tupla<Integer, Integer>> vecinos, int fila, int columna) {
		int filaVecino = fila;
		int columnaVecino = columna - 1;
		
		// Si el vecino no existe devuleve el map actual de vecinos
		if(!this.existe(filaVecino, columnaVecino)) {
			return vecinos;
		}
		
		// Agrego el vecino izquierdo
		vecinos.add(new Tupla<Integer, Integer>(filaVecino, columnaVecino));
		
		return vecinos;
	}
	
	/**
	 * Agrega el vecino inferior del elemento definido por la fila y la columna en el Map de vecinos si existe
	 * 
	 * @param vecinos Map de vecinos
	 * @param fila Fila en la que se encuentra el elemnto
	 * @param columna Columna en la que se encuentra el elemento
	 * @return Map de vecinos con el vecino inferior si existe
	 */
	private List<Tupla<Integer, Integer>> agregarVecinoInferior(List<Tupla<Integer, Integer>> vecinos, int fila, int columna) {
		int filaVecino = fila + 1;
		int columnaVecino = columna;
		
		// Si el vecino no existe devuleve el map actual de vecinos
		if(!this.existe(filaVecino, columnaVecino)) {
			return vecinos;
		}
		
		// Agrego el vecino inferior
		vecinos.add(new Tupla<Integer, Integer>(filaVecino, columnaVecino));
		
		return vecinos;
	}
	
	/**
	 * Genera una matriz con elementos encendidos de manera aleatoria
	 * 
	 * @param tamano Tamano de la matriz
	 * @return boolean[][] Con elementos en true seleccionados de forma aleatoria
	 */
	private boolean[][] generarMatrizAleatoria(int tamano) {
		Random random = new Random(2);
		boolean[][] matriz = new boolean[tamano][tamano];
		
		for (int i = 0; i < tamano; i++) {
			for (int k = 0; k < tamano; k++) {
				if(random.nextInt() > 1) {
					matriz[i][k] = true;
				}
			}
		}
		
		return matriz;
	}
	
	/**
	 * Genera una matriz con sus elementos alternados estilo: 1 si, otro no, 1 si...<br>
	 * El primer elemento queda en el estado en el que esta actualmente, luego comienza a alternarlos como se menciona anteriormente
	 * 
	 * @param tamano Tama�o de la matriz
	 * @return boolean[][] Matriz de booleanos
	 */
	private boolean[][] generarMatrizAlternada(int tamano) {
		int contador = 0;
		boolean[][] matriz = new boolean[tamano][tamano];
		
		for (int i = 0; i < tamano; i++) {
			for (int k = 0; k < tamano; k++) {
				if(contador % 2 != 0) {
					matriz[i][k] = true;
				}
				contador++;
			}
		}
		
		return matriz;
	}

	/**
	 * Genera una matriz con todos sus elementos en true
	 * 
	 * @param tamano Tamano de la matriz
	 * @return boolean[][] Con todos sus elementos en true
	 */
	private boolean[][] generarMatrizEncendida(int tamano) {
		boolean[][] matriz = new boolean[tamano][tamano];
		
		for (int i = 0; i < tamano; i++) {
			for (int k = 0; k < tamano; k++) {
				matriz[i][k] = true;
			}
		}
		
		return matriz;
	}

	/**
	 * Devuelve un String con 0 donde la posicion del tablero sea False, y 1 donde
	 * la posicion del tablero sea True
	 * 
	 * @return El tablero representado como un String
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < this.tamano; i++) {
			for (int k = 0; k < this.tamano; k++) {
				if (this.getContenido(i, k)) {
					sb.append("1 ");
				} else {
					sb.append("0 ");
				}
			}
			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Compara elemento por elemento los elementos de ambos tableros y compara si tiene el mismo valor
	 * 
	 * @param otro Otro Tablero
	 * @return 0 Si los tableros tienen los mismos elementos en las mismas posiciones
	 * @return -1 Si los tableros no tiene los mismos elementos en las mismas posiciones
	 */
	@Override
	public int compareTo(Tablero otro) {
		boolean sonIguales = true;
		
		for(int i = 0; i < this.tamano(); i++) {
			for(int k = 0; k < this.tamano(); k++) {
				sonIguales = sonIguales && (this.getContenido(i, k) == otro.getContenido(i, k));
			}
		}
		
		if(sonIguales) {
			return 0;
		} else {
			return -1;
		}
	}

	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	public boolean[][] getTablero() {
		return tablero;
	}

	public void setTablero(boolean[][] tablero) {
		this.tablero = tablero;
	}
	
}
