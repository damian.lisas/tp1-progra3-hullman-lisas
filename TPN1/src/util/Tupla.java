package util;

public class Tupla<X, Y> implements Comparable<Tupla<X, Y>> {
	public X x;

	public Y y;

	public Tupla(X x, Y y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		String string = "["+x+", "+y+"]";
		return string;
	}

	/**
	 * Compara esta tupla contra otra.
	 * 
	 * @return 0 Si las tuplas tienen los mismos valores X e Y
	 * @return -1 Si las X son diferentes
	 * @return 1 Si las Y son diferentes
	 */
	@Override
	public int compareTo(Tupla<X, Y> otra) {
		if(this.x == otra.x && this.y == otra.y) {
			return 0;
		} else if(this.x != otra.x) {
			return -1;
		} else {
			return 1;
		}
	}
}
