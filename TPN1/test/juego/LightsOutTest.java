package juego;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import app.juego.LightsOut;
import exception.JuegoTerminadoException;

class LightsOutTest {

	private LightsOut juego;
	
	@BeforeEach
	public void setUp() {
		juego = LightsOut.crearJuegoDePrueba();
	}

	@Test
	@DisplayName("Generacion del tablero de juego")
	void generarTableroValido() {
		juego = new LightsOut(3);
		
		assertTrue(juego.getCantidadLucesEncendidas() > 0);
	}

	@Test
	@DisplayName("Ganar un juego")
	void ganarJuego() throws JuegoTerminadoException {
		juego.jugar(0, 0);
		juego.jugar(2, 2);
		juego.jugar(2, 0);
		juego.jugar(0, 2);
		juego.jugar(1, 1);
		
		assertTrue(juego.hayGanador() == true);
	}
	
	@Test
	@DisplayName("Romper el juego")
	void romperJuego() throws JuegoTerminadoException {
		juego.jugar(0, 0);
		juego.jugar(2, 2);
		juego.jugar(2, 0);
		juego.jugar(0, 2);
		juego.jugar(1, 1);
		
		assertThrows(JuegoTerminadoException.class, () -> {
			juego.jugar(1, 1);
		});
	}

	@Test
	@DisplayName("Puntaje")
	void testPuntaje() throws JuegoTerminadoException {
		int puntajeEsperado = 9760;
		juego.jugar(0, 0);
		
		assertTrue(puntajeEsperado == juego.getPuntaje());
	}
}
