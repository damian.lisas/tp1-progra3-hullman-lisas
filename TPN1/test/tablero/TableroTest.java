package tablero;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import app.logic.tablero.Tablero;
import util.Tupla;

class TableroTest {
	
	private final int TAMANO_TABLEROS = 3;
	private Tablero tablero;
	private List<Tupla<Integer, Integer>> vecinosDelOrigen;
	public static Tablero tableroEncendidoManualmente;
	public static Tablero tableroAlternadoManualmente;
	
	@BeforeAll
	public static void initializeOne() {
		TableroTest.tableroEncendidoManualmente = new Tablero(3);
		for(int i = 0; i < tableroEncendidoManualmente.tamano(); i++) {
			for(int k = 0; k < tableroEncendidoManualmente.tamano(); k++) {
				tableroEncendidoManualmente.alternar(i, k);
			}
		}
		
		TableroTest.tableroAlternadoManualmente = new Tablero(3);
		TableroTest.tableroAlternadoManualmente.alternar(0, 1);
		TableroTest.tableroAlternadoManualmente.alternar(1, 0);
		TableroTest.tableroAlternadoManualmente.alternar(1, 2);
		TableroTest.tableroAlternadoManualmente.alternar(2, 1);
	}

	@BeforeEach
	void initialize(TestInfo testInfo) {
		System.out.println("Testeando... " + testInfo.getDisplayName());
		
		tablero = new Tablero(TAMANO_TABLEROS);
		
		vecinosDelOrigen = new ArrayList<Tupla<Integer, Integer>>();
		vecinosDelOrigen.add(new Tupla<Integer, Integer>(0,1));
		vecinosDelOrigen.add(new Tupla<Integer, Integer>(1,0));
	}
	
	@Test
	@DisplayName("Obtener contenido, caso feliz")
	void getContenidoHappyTest() {
		boolean contenido = tablero.getContenido(0, 0);
		
		assertTrue(contenido == false);
	}

	@SuppressWarnings("unused")
	@Test
	@DisplayName("Obetenr contenido, caso indice negativo")
	void getContenidoIndiceNegativo() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			boolean contenido = tablero.getContenido(-1, 1);
		});
	}
	
	@SuppressWarnings("unused")
	@Test
	@DisplayName("Obtener contenido, caso borde")
	void getContenidoIndiceGrande() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			boolean contenido = tablero.getContenido(tablero.tamano(), 1);
		});
	}
	
	@Test
	@DisplayName("Alternar")
	void alternarTest() {
		boolean estadoInicial = tablero.getContenido(0, 0);
		
		tablero.alternar(0, 0);
		
		assertTrue(tablero.getContenido(0, 0) != estadoInicial);
	}
	
	@Test
	@DisplayName("Alternar con vecinos")
	void alternarConVecinosTest() {
		tablero.alternarConVecinos(1, 1);
		
		assertTrue(tablero.getContenido(0, 1));
		assertTrue(tablero.getContenido(1, 0));
		assertTrue(tablero.getContenido(1, 1));
		assertTrue(tablero.getContenido(1, 2));
		assertTrue(tablero.getContenido(2, 1));
		
		assertFalse(tablero.getContenido(0, 0));
	}
	
	@Test
	@DisplayName("Obtener vecinos")
	void getVecinosTest() {
		List<Tupla<Integer, Integer>> vecinos = tablero.getVecinos(0, 0);
		
		assertTrue(vecinos.get(0).compareTo(vecinosDelOrigen.get(0)) == 0);
		assertTrue(vecinos.get(1).compareTo(vecinosDelOrigen.get(1)) == 0);
		assertThrows(IndexOutOfBoundsException.class, () -> {
			vecinos.get(2);
		});
	}
	
	@Test
	@DisplayName("CompareTo")
	void compareToTest() {
		assertTrue(tablero.compareTo(tablero) == 0);
	}
	
	@Test
	@DisplayName("Generacion aleatoria")
	void generacionAleatoria() {
		Tablero tableroAleatorio = new Tablero(TAMANO_TABLEROS, Tablero.GENERACION.ALEATORIA);
		
		assertTrue(tablero.compareTo(tableroAleatorio) != 0);
	}
	
	@Test
	@DisplayName("Generacion encendido")
	void generacionEncendido() {
		Tablero tableroEncendidoGenerado = new Tablero(TAMANO_TABLEROS, Tablero.GENERACION.ENCENDIDO);
		
		assertTrue(tableroEncendidoGenerado.compareTo(tableroEncendidoManualmente) == 0);
	}
	
	@Test
	@DisplayName("Generacion alternado")
	void generacionAlternado() {
		Tablero tableroAlternado = new Tablero(TAMANO_TABLEROS, Tablero.GENERACION.ALTERNADO);
		
		assertTrue(tableroAlternado.compareTo(tableroAlternadoManualmente) == 0);
	}
	
	@Test
	@DisplayName("Contar elementos en true")
	void contarElementosEnTrueTest() {
		tablero = new Tablero(TAMANO_TABLEROS, Tablero.GENERACION.ENCENDIDO);
		
		int cantidadEnTrue = tablero.contarPosicionesEnTrue();
		
		assertTrue(cantidadEnTrue == (TAMANO_TABLEROS * TAMANO_TABLEROS));
	}

}
